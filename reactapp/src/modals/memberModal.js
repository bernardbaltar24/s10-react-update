import React, {useEffect, useState} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input, Form } from 'reactstrap';

const MemberModal = ({modal, toggle, member, teams, updateMember}) => {

	const [memberModal, setMemberModal] = useState({
		teamId: "",
		position: "",
		teams: []
	})

	const {teamId, position, teams2} = memberModal;

	useEffect(() => {
		setMemberModal({
			teamId: member.teamId ? member.teamId._id : null,
			position: member.position ? member.position : null,
			teams2: teams
		})
	}, [toggle])

	 //POPULATE DROPDOWN

  const populateTeams = () => {
    return teams.map(team => {
      return (
        <option key={team._id} value={team._id} selected = {teamId === team._id ? true : false} >
        {team.name}
        </option>
        )
    })
  }

  // IF TEAMS IS NULL

  let na;
  if(teamId === null || position === null){
  	na = (
  		<option selected disabled>Please select a team...</option>
  		)
  }

  //HANDLE CHAMGES TO FORM
  const onChangeHandler = (e) =>{ //to target id of input.
  	setMemberModal({
  		...memberModal,
  		[e.target.name] : e.target.value
  	})
  }

  //HANDLE SUBMIT
  const onSubmitHandler = (e) => {
  	e.preventDefault()

  	const updates = {
  		teamId,
  		position
  	}

  	updateMember(member._id, updates)
  }

	return (
		<Modal isOpen={modal} toggle={toggle} >
			<ModalHeader toggle={toggle} >Update Member</ModalHeader>
			<ModalBody>
				<Form className="border rounded" onSubmit={e=>onSubmitHandler(e)} >
				<FormGroup>
				  <Label for="username" className="mt-3">Username</Label>
				  <Input type="text" name="username" id="username" placeholder="" value={member.username} disabled />
				</FormGroup>
				<FormGroup>
				   	<Label for="teamId" className="mt-3">Team</Label>
				  	<Input type="select" name="teamId" id="teamId" onChange={e=>onChangeHandler(e)} > 
				  		{na}
			        	{populateTeams()}
			        </Input>
				</FormGroup>
				<FormGroup>
				  <Label for="position" className="mt-3">Position</Label>
				  <Input type="select" name="position" id="position" placeholder="" onChange={e=>onChangeHandler(e)} >
				  {na}
				  <option value="admin" selected={member.position === "admin" ? true:false} >Admin</option>
				  <option value="ca" selected={member.position === "ca" ? true:false} >CA</option>
				  <option value="student" selected={member.position === "student" ? true:false} >Student</option>
				  <option value="hr" selected={member.position === "hr" ? true:false} >HR</option>
				  <option value="instructor" selected={member.position === "instructor" ? true:false} >Instructor</option>
				  </Input>
				</FormGroup>

				<Button className="">Save Changes</Button>
				</Form>
			</ModalBody>
			<ModalFooter></ModalFooter>
		</Modal>
	)
}

export default MemberModal;