import React from 'react';
import { Table } from 'reactstrap';
import TeamsTableBody from '../rows/TeamsTableBody';

const TaskTables = (props) => {
console.log(props)
  let row;

    if(!props.teamsAttr){ //pwedeng ipasok ang if sa loob ng return by using ternary = !props.teamAttr?statement:result
      row = (
        <tr colSpan="3">
          <em>No Teams found..</em>
        </tr>
        )
    }else{
      let i = 0;
      row=(
        props.teamsAttr.map( teams => {return <TeamsTableBody teamsAttr={teams} key={teams._id} index={++i} deleteTeam={props.deleteTeam} />
        })
      )
    }

  return (
    <Table responsive hover borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Team Name</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {row}
      </tbody>
    </Table>
  );
}

export default TaskTables;