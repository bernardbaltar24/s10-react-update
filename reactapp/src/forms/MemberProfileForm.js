import React, {useState, useEffect} from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const MemberProfileForm = ({member, props}) => {

	const {firstName, lastName, username, email, teamId, position} = member

  const [setMember, setMemberData] = useState({
      teams: [],
      teamId2: "",
      position2: ""
    })

  const {teams, teamId2, position2} = setMember;

  useEffect(() => {
    setMemberData({
      teams:props.teams,
      teamId2: teamId2 ? teamId2._id : null,
      position2: position2 ? position2 : null,
    })
  }, [props])

  //-----------POPULATE-------------

  const populateTeams = () => {
    return teams.map(team => {
      return(
        <option key={team._id} value={team._id} selected={teamId === null ? false : teamId2 ? true : false} >{team.name}</option>
      )
    })
  }
  

  return (
    <Form>
      <FormGroup>
        <Label for="firstname" className="mt-3">First Name</Label>
        <Input type="text" name="firstname" id="firstname" placeholder="with a placeholder" value={firstName} />
      </FormGroup>
      <FormGroup>
        <Label for="lastname" className="mt-3">Last Name</Label>
        <Input type="text" name="lastname" id="lastname" placeholder="with a placeholder" value={lastName} />
      </FormGroup>
      <FormGroup>
        <Label for="username" className="mt-3">Username</Label>
        <Input type="text" name="username" id="username" placeholder="with a placeholder" value={username} />
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email" placeholder="password placeholder" value={email} />
      </FormGroup>
      <FormGroup>
        <Label for="team">Team</Label>
        <Input type="select" name="team" id="teamId2" value={teamId2} />
        {populateTeams()}
      </FormGroup>
      <FormGroup>
        <Label for="position">Position</Label>
        <Input type="text" name="position" id="position" placeholder="password placeholder" value={position} />
      </FormGroup>

      <div className="text-center"><Button className="mb-3 primary">Save Changes</Button></div>
    </Form>
  );
}

export default MemberProfileForm;