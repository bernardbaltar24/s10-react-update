import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberProfileForm from '../forms/MemberProfileForm';
import axios from 'axios';



const MemberProfilePage = (props) => {

  const [memberData, setMemberData] = useState({
    token: props.token,
    member: {}
  })

  const {token, member} = memberData

//----------GET MEMBER-------------
  const getMember = async () => {
    try{
      const config={
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
      const res = await axios.get(`http://localhost:5000/members/${props.match.params.id}`, config)
      setMemberData({
        ...memberData,
        member: res.data
      })
    } catch(e){
      console.log(e)
    }
  }

  useEffect(()=>{
    getMember()
  }, [setMemberData])

  //--------------GET ALL TEAMS-------------

    const [teams, setTeams] = useState([])

    const getTeams = async() => {
      try{
        const config = {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
        const res = await axios.get("http://localhost:5000/teams", config)

        setTeams(res.data)
      }catch(e){
        console.log(e)
      }
    }

    useEffect(()=>{
      getTeams()
    }, [setTeams])

  //--------------POPULATE DROPDOWN---------

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Member Profile Page</h1>
        </Col>
      </Row>
      <Row className="mb-5">
        <Col>
          <MemberProfileForm member={member} teams={teams} />
        </Col>
      </Row>

    </Container>
  );
}

export default MemberProfilePage;