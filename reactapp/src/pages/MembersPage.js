import React, {useState, useEffect, Fragment} from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import MemberForm from '../forms/MemberForm';
import MemberTableHead from '../tables/MemberTableHead';
import MemberModal from '../modals/memberModal'
import axios from 'axios';


const MembersPage = (props) => {

  console.log("MEMBERSPAGE TOKEN", props.token)

    const[membersData, setMembersData] = useState({
      token: props.token,
      members: []
    })

    //destructure
    const {token, members} = membersData;

    const config = {
        headers : {
          Authorization: `Bearer ${token}`
        }
      }

    const url = `http://localhost:5000`

//------------------getmembers-----------------

    const getMembers = async (query="") => {
      try{
       
        const res = await axios.get(`http://localhost:5000/members${query}`, config)
        console.log("MEMBERS PAGE res", res)

        return setMembersData({
          ...membersData,
          members: res.data
        })
      }catch(e){
        console.log("MEMBERS ERROR", e)
      }
    }

//------------------SOFT DELETE MEMBER--------------------

    const deleteMember = async(id) => {
      try{

        const res = await axios.delete(`/${id}`, config)

        getMembers()

      } catch(e){
        console.log("ERROR SA DELETE MEMBER", e)
      }
    }
  
  useEffect(() => {
    getMembers()
  },[setMembersData])
  
//--------------------UPDATE MEMBER MODAL---------------------

  const [modalData, setModalData] = useState({
    modal: false,
    member: {}
  })

  const {modal, member} = modalData;

  const toggle = async (id) => {
    console.log("ID ng MODAL SA MEMBER", id);

    try{
      if(typeof id === 'string'){

        const res = await axios.get(`http://localhost:5000/members/${id}`, config);

        return setModalData({
          modal: !modal,
          member: res.data
        })
      
    }

    return setModalData({
      ...modalData,
      modal: !modal
    })

    } catch(e) {
      console.log(e.response)
    }
  }

  //----------------GET ALL TEAMS----------------------

  const [teams, setTeams] = useState([]) //since isa lang idedeclare we can use empty array

  const getTeams = async() => {
    try{
      const res = await axios.get(`${url}/teams`, config)

      setTeams(res.data)
    }catch(e){
      console.log(e)
    }
  }

  useEffect(() => {
    getTeams()
  }, [setTeams])

 //UPDATE MEMBER

 const updateMember = async (id, updates) => {
  console.log(id, updates)
  try{
    config.headers["Content-Type"] = "application/json"

    const body = JSON.stringify(updates);

    const res = await axios.patch(`${url}/members/${id}`, body, config)

    setModalData({
      ...modalData,
      modal: !modal
    })
    getMembers()

    //SWAL

  }catch(e){
    console.log(e)
  }
 }

  return (
    <Fragment>
      <Container>
        <Row className="mb-5">
          <Col>
          	<h1>MEMBERS Page</h1>
          </Col>
        </Row>
        <Row className="d-flex">
          <Col md="4" className="border">
          	<MemberForm/>
          </Col>
          <Col>
            <div>
              <Button className="btn-sm border mr-1" onClick={()=>getMembers()}>
                Get All
              </Button>
              <Button className="btn-sm border mr-1" onClick={()=>getMembers("?isActive=true")}>
                Get Active
              </Button>
              <Button className="btn-sm border mr-1" className="btn-sm border mr-1" onClick={()=>getMembers("?isActive=false")}>
                Get Deactive
              </Button>
            </div>
            <hr/>
          	<MemberTableHead membersAttr={members} deleteMember={deleteMember} toggle={toggle} />
          </Col>
        </Row>
      </Container>
      <MemberModal modal={modal} toggle={toggle} member={member} teams={teams} updateMember={updateMember} />
      </Fragment>
  );
}

export default MembersPage;